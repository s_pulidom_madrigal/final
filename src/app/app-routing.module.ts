import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from './principal/principal.component';
import { SitiosDeInteresComponent } from './sitios-de-interes/sitios-de-interes.component';
import { MapaComponent } from './mapa/mapa.component';
import { FormularioComponent } from './formulario/formulario.component';

const routes: Routes = [{path:'principal', component:PrincipalComponent},{path:'sitios',component:SitiosDeInteresComponent},{path:'mapa', component:MapaComponent},{path:'formulario',component:FormularioComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
