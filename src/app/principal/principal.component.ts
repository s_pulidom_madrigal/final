import { Component, OnInit, NgModule } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Routes, Router, RouterModule } from '@angular/router';
import { SitiosDeInteresComponent } from '../sitios-de-interes/sitios-de-interes.component';

const routes: Routes = [{path:'Sitios', component: SitiosDeInteresComponent}];

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})

export class PrincipalComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  Sitios(){
    console.log("Funciona");
    this.router.navigateByUrl('/sitios');
  }
  formulario(){
    console.log("Funciona");
    this.router.navigateByUrl('/formulario');
  }
  
  
}
