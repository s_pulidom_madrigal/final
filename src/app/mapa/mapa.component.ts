import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
	var mymap = L.map('mapid').setView([4.628265271905265,-74.06593769788742], 13);

  var portal80 = L.icon({
  iconUrl: 'assets/portal80.jpg',
  
  iconSize:     [100, 80], // size of the icon
  iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor

  
});

var Marker = L.icon({
  iconUrl: 'assets/marcador.png',
  
  iconSize:     [20, 20], // size of the icon


  
});

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
  maxZoom: 18,
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1
}).addTo(mymap);

  L.marker([4.710685, -74.112215], {icon: portal80}).addTo(mymap)
.bindPopup("Centro comercial Portal 80").openPopup();
L.marker([4.694772, -74.086188],{icon:Marker} ).addTo(mymap)
.bindPopup("Centro comercial Titan plaza").openPopup();
L.marker([4.727882, -74.126217],{icon:Marker} ).addTo(mymap)
.bindPopup("'Puente de guaduas").openPopup();
L.marker([4.712864, -74.110968],{icon:Marker} ).addTo(mymap)
.bindPopup("'Parque San Andres").openPopup();
L.marker([4.694608, -74.092657],{icon:Marker} ).addTo(mymap)
.bindPopup("Parque Santamaria del lago").openPopup();
L.marker([4.733651, -74.115549],{icon:Marker} ).addTo(mymap)
.bindPopup("Parque siete Canchas").openPopup();
L.marker([4.732934, -74.143092],{icon:Marker} ).addTo(mymap)
.bindPopup("Parque de la Florida").openPopup();
L.marker([4.710688, -74.108069],{icon:Marker} ).addTo(mymap)
.bindPopup("Iglesia Nuestro señor de los milagros").openPopup();
L.marker([4.722860, -74.113522],{icon:Marker} ).addTo(mymap)
.bindPopup("Centro Comercial Unicentro de occidente").openPopup();
L.marker([4.700586, -74.115075],{icon:Marker} ).addTo(mymap)
.bindPopup("Centro Comercial Diver plaza").openPopup();
L.marker([4.665949, -74.094868],{icon:Marker}).addTo(mymap)
.bindPopup("Coliseo el Salitre").openPopup();
L.marker([4.667874, -74.100045],{icon:Marker} ).addTo(mymap)
.bindPopup("Jardin Botanico Jose Celestino Mutis").openPopup();
L.marker([4.657714, -74.104693],{icon:Marker} ).addTo(mymap)
.bindPopup("Sede periodico el tiempo").openPopup();
L.marker([4.658793, -74.105591],{icon:Marker} ).addTo(mymap)
.bindPopup("Contraloria").openPopup();
L.marker([4.707068, -74.109266],{icon:Marker} ).addTo(mymap)
.bindPopup("Barrio Quirigua").openPopup();

L.polygon([
  [4.653935395790708,-74.10346984863281 ],
  [4.6927729768973405,-74.06656265258789],
  [4.699787464310309,-74.06930923461914],
      [4.70235372255946,-74.08269882202148],
      [4.734858840307975,-74.11531448364258],
      [ 4.740162162235148,-74.12612915039062],
      [ 4.730753014760978, -74.12818908691406],
      [4.731095167820164,-74.13763046264648],
      [4.725791776407452,-74.15719985961914],
      [4.720659423497786,-74.15977478027344],
      [4.692088632886019,-74.12063598632812],
      [4.686956031423626,-74.12544250488281],
      [4.653935395790708,-74.10346984863281]

]).addTo(mymap).bindPopup("localidad de engativa");


var popup = L.popup();

function onMapClick(e) {
  popup
    .setLatLng(e.latlng)
    .setContent("You clicked the map at " + e.latlng.toString())
    .openOn(mymap);
}

mymap.on('click', onMapClick);
  }

}
