import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  direccion;
  reseña;

  constructor(private http: HttpClient) { }

  ngOnInit() {      

  }
  insertarDatos(){
    const nombre= ((document.getElementById("nombre") as HTMLInputElement).value);
    const correo= ((document.getElementById("correo") as HTMLInputElement).value);
    const direccion= ((document.getElementById("direccion") as HTMLInputElement).value);
    const reseña= ((document.getElementById("reseña") as HTMLInputElement).value);


    let datos={            
      nombre:nombre,
      correo:correo,
      direccion:direccion,
      reseña: reseña
      
  }
  var datosJson = JSON.stringify(datos);
  console.log(datosJson);

 

  fetch('http://localhost:4000/insertar', {
    method: 'POST', 
    body: JSON.stringify(datos),
    headers:{
      'Content-Type': 'application/json'
    }
  });

  }

}
